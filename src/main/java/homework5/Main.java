package homework5;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"read books", "write templates for book", "hang up with Stewie"};
        String [][] scheduleOfChild = {
                {"Monday","Kill Lois"},
                {"Tuesday", "Organize funeral for Lois's death"},
        };
        String[][] scheduleOfFather = {
                {"Sunday", "Go to Clam"}
        };
        String[][] scheduleOfMother = {
                {"Friday", "Do the laundry"},
                {"Sunday", "Go to church"}
        };

        Human mother = new Human("Lois", "Griffin", 1958,65, scheduleOfMother);
        Human father = new Human("Peter", "Griffin", 1956,10, scheduleOfFather);
        Pet pet = new Pet("Dog","Brian",10,88, habits);
        Human child = new Human("Stewie","Griffin",1997, 185, scheduleOfChild);

        Family family = new Family(mother, father, 1, pet);
        System.out.println(family.addChild(child) ? "Child added\n" : "Child could not be added\n");

        System.out.println("[FAMILY toString]\n" + family.toString() + "\n");
        System.out.println("[PET toString]\n" + pet.toString() + "\n");
        System.out.println("[CHILD toString]\n" + child.toString());

        System.out.println("\n[EQUALS]");
        System.out.println("Is father and child are the same? " +child.equals(father));
        System.out.println("Is child and child are the same? " + child.equals(child));

        System.out.println("\n[HASHCODE]");
        System.out.println("hasCode of child: " + child.hashCode());
        System.out.println("hasCode of pet: " + pet.hashCode());

        // false index
        System.out.println(deletionByIndex(family, 7));
        // true index
        System.out.println(deletionByIndex(family, 0));

        // in order to code block run below, please, comment out the code block above
        //so that, there will be a child to be deleted

        // false object
        System.out.println(deletionByObject(family, father));
        // true object
        System.out.println(deletionByObject(family, child));
    }

    public static StringBuilder deletionByIndex(Family family, int indexOfChildrenArray){
        StringBuilder statement = new StringBuilder();
        statement.append("\n[DELETING CHILD FROM FAMILY BY INDEX]");
        if(family.countFamily() - 2 == 0)
            statement.append("\nThere are no child in the family to delete");
        else {
            statement.append("\n- Number of members in the family BEFORE DELETION: " + family.countFamily())
                    .append( family.deleteChildByIndex(indexOfChildrenArray) ? "\nChild deleted" : "\nChild could not be deleted")
                    .append("\n- Number of members in the family AFTER DELETION: " + family.countFamily());
        }
        return statement;
    }

    public static StringBuilder deletionByObject(Family family, Human child){
        StringBuilder statement = new StringBuilder();
        statement.append("\n[DELETING CHILD FROM FAMILY BY OBJECT]");
        if(family.countFamily() - 2 == 0)
            statement.append("\nThere are no child in the family to delete");
        else {
            statement.append("\n- Number of members in the family BEFORE DELETION: " + family.countFamily())
                    .append( family.deleteChildByObject(child) ? "\nChild deleted" : "\nChild could not be deleted")
                    .append("\n- Number of members in the family AFTER DELETION: " + family.countFamily());
        }
        return statement;
    }
}