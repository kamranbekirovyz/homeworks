package homework5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private int indexForChildrenArray = 0;

    // advanced
    static{
        // static method
        System.out.println("Family class is loaded\n");
    }

    {
        // non-static method
        System.out.println("A new family object is created by constructor\n");
    }

    // constructors
    public Family(Human mother,Human father,int NumberOfChildren, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[NumberOfChildren];
        this.pet = pet;
    }

    // methods
    public void welcomeFavourite(){
        System.out.println("Welcome, " + this.pet.getNickname());
    }

    public void describePet(){
        String slyness = this.pet.getTrickLevel() >= 50 ? "very sly" : "almost not sly";
        System.out.println("As a family we have a " + this.pet.getSpecies() +
                ", he is " + this.pet.getAge() + " years old" +
                " he is " + slyness);
    }

    public boolean feedPet(boolean isItTimeForFeeding){
        boolean isPetFed = false;
        if(isItTimeForFeeding){
            System.out.println("Hm... I will feed our family's " + pet.getNickname());
            isPetFed = true;
        } else {
            Random rnd = new Random();
            int randomNumber = rnd.nextInt(100);
            if(pet.getTrickLevel() == randomNumber) {
                System.out.println("Hm... I will feed our family's " + pet.getNickname());
            } else {
                System.out.println("I think Jack is not hungry.");
            }
        }
        return isPetFed;
    }

    public boolean deleteChildByIndex(int indexOfChildrenArray) {
        if(this.children==null || indexOfChildrenArray < 0 || indexOfChildrenArray > this.children.length-1) {
            System.out.println("You sent wrong index for child to be deleted");
            return false;
        }
        Human[] children2 = new Human[this.children.length-1];
        for(int i = 0, k = 0; i < children.length; i++) {
            if(i == indexOfChildrenArray)
                continue;
            children2[k++] = this.children[i];
        }
        this.children = children2;
        return true;
    }

    public boolean deleteChildByObject(Human child) {
        int indexOfChildToBeDeleted = -1;
        boolean childExistsInFamily = false;
        for (int idx = 0; idx < children.length; idx++) {
            if (children[idx].equals(child))
                indexOfChildToBeDeleted = idx;
                childExistsInFamily = true;
                break;
        }
        if (childExistsInFamily)
            return deleteChildByIndex(indexOfChildToBeDeleted);
        return false;
    }

    public boolean addChild(Human child) {
        if(indexForChildrenArray > children.length)
            return false;
        else
            children[indexForChildrenArray++]=child;
        return true;
    }

    public String Children(){
        String allChildren = "";
        for(Human child:children)
            allChildren += child.toString();
        return String.format(allChildren);
    }

    public int countFamily() { return 2 + this.children.length; }

    // overrides
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return indexForChildrenArray == family.indexForChildrenArray &&
                Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, indexForChildrenArray);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{\n" +
                " mother=" + mother.toString() +",\n"+
                " father=" + father.toString() +",\n"+
                " children=" + Children() + ",\n" +
                " pet=" + pet +
                "\n}";
    }

    // getters
    public Human getMother() { return mother; }

    public Human getFather() { return father; }

    public Pet getPet() { return pet; }

    public Human[] getChildren() { return children; }


    // setters
    public void setChildren(Human[] children) { this.children = children; }

    public void setFather(Human father) { this.father = father; }

    public void setMother(Human mother) { this.mother = mother; }

    public void setPet(Pet pet) { this.pet = pet; }

}