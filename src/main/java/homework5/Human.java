package homework5;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;

    // constructors
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    // methods
    public String schedule(){
        String s = "";
        for(int i=0;i<schedule.length;i++)
        {
            s += "[";
            s += schedule[i][0];
            s += ", ";
            s += schedule[i][1];
            s += "]";
        }
        return s;
    }

    // overrides
    @Override
    public String toString() {
        return String.format("Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule() +
                '}');
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    // gettters
    public String getName() { return name; }

    public int getYear() { return year; }

    public String getSurname() { return surname; }

    public int getIq() { return iq; }

    public String[][] getSchedule() { return schedule; }

    // setters
    public void setSurname(String surname) { this.surname = surname; }

    public void setYear(int year) { this.year = year; }

    public void setName(String name) { this.name = name; }

    public void setIq(int iq) { this.iq = iq; }

    public void setSchedule(String[][] schedule) { this.schedule = schedule; }

}