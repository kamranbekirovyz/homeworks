package homework6;

public enum Species {
    Dog(false, 4, true),
    Unicorn(true, 4, true),
    Cat(false,4,true);

    // fields
    private  boolean canfly;
    private int numberOfLegs;
    private boolean hasFur;

    // constructors
    Species() {
    }

    Species(boolean canfly, int numberOfLegs, boolean hasFur){
        this.canfly=canfly;
        this.numberOfLegs=numberOfLegs;
        this.hasFur=hasFur;
    }

    // overrides
    @Override
    public String toString() {
        return "Pet{canfly=" + canfly +
                ", numberOflegs=" + numberOfLegs +
                ", hasFur=" + hasFur +
                "}";
    }
}
