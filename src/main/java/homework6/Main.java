package homework6;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"read books", "write templates for book", "hang up with Stewie"};
        String [][] scheduleOfChild = {
                {DayOfWeek.Monday.name(),"Kill Lois"},
                {DayOfWeek.Friday.name(), "Organize funeral for Lois's death"}
        };
        String[][] scheduleOfFather = {
                {DayOfWeek.Monday.name(), "Go to Clam"}
        };
        String[][] scheduleOfMother = {
                {DayOfWeek.Sunday.name(), "Do the laundry"},
                {DayOfWeek.Friday.name(), "Go to church"}
        };

        Human mother = new Human("Lois", "Griffin", 1958,65, scheduleOfMother);
        Human father = new Human("Peter", "Griffin", 1956,10, scheduleOfFather);
        Pet pet = new Pet(Species.Dog,"Brian",10,88, habits);
        Human child = new Human("Stewie","Griffin",1997, 185, scheduleOfChild);

        Family family = new Family(mother, father, 1, pet);
        family.addChild(child);

        System.out.println("[FAMILY toString]\n" + family.toString() + "\n");
        System.out.println("[PET toString]\n" + pet.toString() + "\n");
        System.out.println("[CHILD toString]\n" + child.toString());

        System.out.println("\n[EQUALS]");
        System.out.println("Is father and child are the same? " +child.equals(father));
        System.out.println("Is child and child are the same? " + child.equals(child));

        System.out.println("\n[HASHCODE]");
        System.out.println("hasCode of child: " + child.hashCode());
        System.out.println("hasCode of pet: " + pet.hashCode());

        deletion(family, 0);
        deletion(family, 0);

        System.out.println("\n[DELETING CHILD FROM FAMILY BY OBJECT]");

        System.out.println(family.deleteChildByObject(child) ? "Child deleted" : "Child not deleted");
        System.out.println(family.deleteChildByObject(child) ? "Child deleted" : "Child not deleted");
        System.out.println();

        System.gc();
    }

    public static void deletion(Family family, int indexOfChildrenArray){
        System.out.println("\n[DELETING CHILD FROM FAMILY BY INDEX]");
        if(family.countFamily() - 2 == 0)
            System.out.println("There are no child in the family to delete");
        else {
            System.out.println("- Number of members in the family BEFORE DELETION: " + family.countFamily());
            if (family.deleteChildByIndex(indexOfChildrenArray))
                System.out.println("Child deleted");
            System.out.println("- Number of members in the family AFTER DELETION: " + family.countFamily());
        }
    }
}