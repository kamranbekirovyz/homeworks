package homework6;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    private int indexForChildrenArray = 0;
    int indexOfChildToBeDeleted;

    public Family(Human mother,Human father,int NumberOfChildren, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[NumberOfChildren];
        this.pet = pet;
    }

    public void addChild(Human child) {
        if(indexForChildrenArray > children.length)
            throw new ArrayIndexOutOfBoundsException("no more children");
        else
            children[indexForChildrenArray++]=child;
    }

    // methods
    public boolean deleteChildByIndex(int indexOfChildrenArray) {
        if(indexOfChildrenArray < 0 || indexOfChildrenArray > this.children.length-1)
            return false;
        Human[] children2 = new Human[this.children.length-1];
        for(int i = 0, k = 0; i < children.length; i++) {
            if(i == indexOfChildrenArray)
                continue;
            children2[k++] = this.children[i];
        }
        this.children = children2;
        return true;
    }

    public boolean deleteChildByObject(Human child){
        for(int idx = 0; idx < children.length; idx++){
            if (children[idx].equals(child)){
                indexOfChildToBeDeleted = idx;
                break;
            } else {
                return false;
            }
        }
        if(deleteChildByIndex(indexOfChildToBeDeleted))
            return true;
        else
            return false;
    }

    public String Children(){
        String allChildren = "";
        for(Human child:children)
            allChildren += child.toString();
        return String.format(allChildren);
    }

    public int countFamily() { return 2 + this.children.length; }

    // overrides
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return indexForChildrenArray == family.indexForChildrenArray &&
                Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, indexForChildrenArray);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{\n" +
                " mother=" + mother.toString() +",\n"+
                " father=" + father.toString() +",\n"+
                " children=" + Children() + ",\n" +
                " pet=" + pet +
                "\n}";
    }

    @Override
    protected void finalize() throws Throwable{
        System.out.println("Finalizing done in Family class");
    }

    // getters
    public Human getMother() { return mother; }

    public void setMother(Human mother) { this.mother = mother; }

    public Human getFather() { return father; }

    public void setFather(Human father) { this.father = father; }

    public Human[] getChildren() { return children; }

    // setters
    public void setChildren(Human[] children) { this.children = children; }

    public Pet getPet() { return pet; }

    public void setPet(Pet pet) { this.pet = pet; }

    public int getIndexForChildrenArray() { return indexForChildrenArray; }

    public void setIndexForChildrenArray(int indexForChildrenArray) { this.indexForChildrenArray = indexForChildrenArray; }
}