package homework7;

public class DomesticCat extends Pet{
    Species species;

    public DomesticCat(Species species){
        this.species = species;
    }

    @Override
    public void respond(String nickname) {
        System.out.printf("My name is %s and I am never listening to my owner", nickname);
    }

    @Override
    public String toString() {
        return "Domestic Cat{" + "species =" + species + "}";
    }
}