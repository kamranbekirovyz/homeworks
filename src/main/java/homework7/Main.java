package homework7;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"invent technology", "call Peter fat ass", "hate dog - (I mean meg)"};
        String [][] schedule = {
                {"Monday","Kill Lois"},
                {"Tuesday", "Organize funeral for Lois's death"},
                {"Wednesday", "go to North Pole with Brian to see Santa"},
                {"Thursday", "fix the time machine"},
                {"Friday", "start learning American Accent"},
                {"Saturday", "Shoot footage for 'True Colors'"},
                {"Sunday", "see if you can make meg the dog cry"}
        };

        Human mother = new Human("Lois", "Griffin", 1958,45, schedule);
        Human father = new Human("Peter", "Griffin", 1956,35, schedule);
        //Pet pet = new Pet(Species.Dog, "Brian", 3, 100, habits);
        Human child = new Human("Stewie","Griffin",1997,175, schedule);

        Family family = new Family(mother, father, 1);
        family.addChild(child);
        System.out.println(family.toString());
        // System.out.println(pet.toString());
        System.out.println(child.toString());
    }
}