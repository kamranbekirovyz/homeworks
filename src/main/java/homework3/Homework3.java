package homework3;

import java.util.Scanner;

public class Homework3 {
    public static void main(String[] args) {
        String[][] schedule = {
                {"Sunday", "do homework"},
                {"Monday", "go to courses; watch a film"},
                {"Tuesday", "play tennis with peter"},
                {"Wednesday", "start on learning Spanish"},
                {"Thursday", "finish the powerpoint for economics class"},
                {"Friday", "do homework"},
                {"Saturday", "realize that tomorrow is monday"},
        };
        Scanner myObject = new Scanner(System.in);
        String input;
        boolean exit = false;
        boolean change = false;

        while(exit == false){
            input = myObject.nextLine();
            // checks if input contains change and not contains only the word "change"
            if(input.contains("change") && input.length() > "change".length() + 2){
                change = true;
                // cut the change part so that only weekday remains
                input = input.substring("change".length() + 1, input.length());
            }

            input =(input.substring(0, 1)).toUpperCase() + (input.substring(1)).toLowerCase();
            // checks if user put "ONE" space at the end of input
            if(input.endsWith(" ")) input = input.substring(0, input.length() - 1);

            switch(input){
                case "Sunday":
                    if(change) {
                        System.out.println("Current task: " + schedule[0][1]);
                        System.out.print("Update with: ");
                        schedule[0][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[0][1]);
                    break;
                case "Monday":
                    if(change) {
                        System.out.println("Current task: " + schedule[1][1]);
                        System.out.print("Update with: ");
                        schedule[1][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[1][1]);
                    break;
                case "Tuesday":
                    if(change) {
                        System.out.println("Current task: " + schedule[2][1]);
                        System.out.print("Update with: ");
                        schedule[2][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[2][1]);
                    break;
                case "Wednesday":
                    if(change) {
                        System.out.println("Current task: " + schedule[3][1]);
                        System.out.print("Update with: ");
                        schedule[3][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[3][1]);
                    break;
                case "Thursday":
                    if(change) {
                        System.out.println("Current task: " + schedule[4][1]);
                        System.out.print("Update with: ");
                        schedule[4][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[4][1]);
                    break;
                case "Friday":
                    if(change) {
                        System.out.println("Current task: " + schedule[5][1]);
                        System.out.print("Update with: ");
                        schedule[5][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[5][1]);
                    break;
                case "Saturday":
                    if(change) {
                        System.out.println("Current task: " + schedule[6][1]);
                        System.out.print("Update with: ");
                        schedule[6][1] = myObject.nextLine();
                        change = false;
                    }
                    else System.out.println(schedule[6][1]);
                    break;

                case "Exit":
                    exit = true;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");

            }
        }
    }

}
