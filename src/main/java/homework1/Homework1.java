package homework1;

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Homework1 {
    public static void main(String[] args) {
        Random random = new Random();
        int randomIndex = random.nextInt(4);
        int inputtedNumber, numberToBeGuessed;
        int count = 0;
        Scanner myObject = new Scanner(System.in);
        int[] arrayOfInputtedNumbers = new int[100];

        String[][] gameData = {
                {"When did the World War II begin?", "1939"},
                {"When did Turkish Republic establish?", "1923"},
                {"When did the World War II finish?", "1945"},
                {"When did the Soviet started participating in World War II?", "1941"},
        };

        System.out.print("Enter you name: ");
        String name = myObject.nextLine();

        System.out.println("Let the game begin!");
        System.out.println(gameData[randomIndex][0]);
        numberToBeGuessed = Integer.parseInt(gameData[randomIndex][1]);

        do {
            while (true) {
                try {
                    String input = myObject.nextLine();
                    inputtedNumber = Integer.parseInt(input);
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("Not a valid input, please input number only.");
                }
            }

            arrayOfInputtedNumbers[count] = inputtedNumber;
            count++;

            if(inputtedNumber > numberToBeGuessed) System.out.println("Your number is too big. Please, try again.");
            else if(inputtedNumber < numberToBeGuessed) System.out.println("Your number is too small. Please, try again.");
        } while (inputtedNumber != numberToBeGuessed);

        System.out.println("Congratulations " + name + "!");
        Arrays.sort(arrayOfInputtedNumbers);
        System.out.print("Your numbers: ");
        for(int index = 99; index >= 100-count; index--) System.out.print(arrayOfInputtedNumbers[index] + " ");
    }
}