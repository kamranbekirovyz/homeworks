package homework4;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"read books", "write templates for book", "hang up with Stewie"};
        String [][] schedule = {
                {"Monday","Kill Lois"},
                {"Tuesday", "Organize funeral for Lois's death"},
                {"Wednesday", "go to North Pole with Brian to see Santa"},
                {"Thursday", "fix the time machine"},
                {"Friday", "start learning American Accent"},
                {"Saturday", "Shoot footage for 'True Colors'"},
                {"Sunday", "see if you can make meg the dog cry"}
        };

        Human mother = new Human(
                "Lois",
                "Griffin",
                1958
        );
        Human father = new Human(
                "Peter",
                "Griffin",
                1956
        );
        Pet pet = new Pet(
                "Dog",
                "Brian",
                7,
                99,
                habits
        );
        Human child = new Human(
                "Stewie",
                "Griffin",
                1997,
                mother,
                father,
                pet,
                179,
                schedule
        );

        System.out.println(pet.toString());
        System.out.println(child.toString());
        System.out.println(child.feedPet(false));
    }
}
