package homework4;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;
    String[] habits;

    // constructors
    public Pet(String species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    public Pet(){

    }

    // methods
    void eat(){

    }
    void respond(){

    }
    void foul(){

    }
    @Override
    public String toString() {
        return String.format(this.species+ "{" + "nickname" +
                this.nickname + ", " + "age" + "=" + this.age + ", " + "trickLevel" + "=" +
                this.trickLevel + ", " + "habits" + "=" + Arrays.toString(this.habits) + "}");
    }
}
