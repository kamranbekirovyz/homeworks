package homework4;

import java.util.Arrays;
import java.util.Random;

public class Human {
    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

    // constructors
    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }
    public Human(String name, String surname, int year, Human mother, Human father, Pet pet, int iq, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.iq = iq;
        this.schedule = schedule;
    }
    public Human(){

    }

    public void greetPet(){
        System.out.println("Hello, " + this.pet.nickname);
    }
    public void describePet(){
        String slyness = this.pet.trickLevel >= 50 ? "very sly" : "almost not sly";
        System.out.println("I have a " + this.pet.species + ", he is " + this.pet.age + " years old" + " he is " + slyness);
    }
    public boolean feedPet(boolean isItTimeForFeeding){
        boolean isPetFed = false;
        if(isItTimeForFeeding){
            System.out.println("Hm... I will feed " + this.name + "'s " + pet.nickname);
            isPetFed = true;
        } else {
            Random rnd = new Random();
            int randomNumber = rnd.nextInt(100);
            if(pet.trickLevel == randomNumber) {
                System.out.println("Hm... I will feed " + this.name + "'s " + pet.nickname);
            } else {
                System.out.println("I think Jack is not hungry.");
            }
        }
        return isPetFed;
    }
    @Override
    public String toString() {
        return String.format("Human" + "{" + "name" + "=" +
                this.name + ", " + "surname" + "=" +
                this.surname + ", " + "year" + "=" +
                this.year + ", " + "iq" + "=" +
                this.iq + ", " + "pet" + "=" +
                pet.toString() + ", " + "mother" + "=" +
                this.mother.name + " " + this.mother.surname + ", " + "father" + "=" +
                this.father.name + " " + this.father.surname + ", " + "schedule" + "=" +
                this.allScheduleElements() + "}");
    }
    public String allScheduleElements(){
        String s = "";
        for (int i = 0; i < schedule.length; i++) {
            s += "{";
            s += schedule[i][0];
            s += ": ";
            s += schedule[i][1];
            s += "}";
        }
        return s;
    }
}
